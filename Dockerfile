FROM squidfunk/mkdocs-material:9.5.25 as build
WORKDIR /build

COPY ./ru/ /build/ru/
COPY ./en/ /build/en/
COPY ./media/ /build/ru/media/
COPY ./media/ /build/en/media/
RUN set -xe \
    && cd /build/ru/ \
    && mkdocs build
RUN set -xe \
    && cd /build/en/ \
    && mkdocs build

FROM nginx:1.23-alpine
COPY ./nginx_default.conf /etc/nginx/conf.d/default.conf
COPY ./media/ /var/www/html/ru/media/
COPY ./media/ /var/www/html/en/media/
COPY --from=build /build/ru/site /var/www/html/ru
COPY --from=build /build/en/site /var/www/html/en
LABEL app.yucca.docs=true

EXPOSE 8080

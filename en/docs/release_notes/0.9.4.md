---
title: What's new in Yucca 0.9.4
description: Video surveillance for home and business
---

# What's new in Yucca 0.9.4

*19.09.2023*

!!! info "Learn more about the features of 0.9.0"
	This is a technical release with fixes, read the review of Yucca innovations 0.9.0 [on this page](/en/release_notes/0.9.0/).

## Other changes

### Fixed

* [UI] The domain and port are cached to configure smpt motion detection when switching the server behind the gateway ([#1144](https://gitlab.com/yuccastream/yucca/-/issues/1144))
* [server] Database migration from sqlite to postgres fails ([#1143](https://gitlab.com/yuccastream/yucca/-/issues/1143))
* [build] 404 on swagger page ([#1142](https://gitlab.com/yuccastream/yucca/-/issues/1142))
* [UI] The player jumps through detection events during the daytime period ([#1049](https://gitlab.com/yuccastream/yucca/-/issues/1049))
* [UI] Translation corrections

## What's next?

Installation instructions can be found in the section [Installation](/en/install/).

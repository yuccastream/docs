---
title: What's new in Yucca 0.9.0
description: Video surveillance for home and business
---

# What's new in Yucca 0.9.0

*06.07.2023*

## Mosaic

Live view mode from several cameras at once.
Our users have been waiting for this feature for a long time and we are happy to finally introduce it.

[![mosaic](/en/media/v090/mosaic.png)](/en/media/v090/mosaic.png)

*Keep in mind that playing video on your computer takes up CPU resources, and playing video from multiple cameras at once can put a lot of load on your device's CPU.*

## Mobile app for Android (BETA)

We are launching open testing of our mobile application for the Android platform.

<a href='https://releases.yucca.app/android/latest/app-yucca-release.apk'><img alt='Download APK' src='/en/media/Download_Android_APK_Badge.png' width="300" /></a>

[![andriod_1](/en/media/v090/andriod_1.jpg){ width="300" }](/en/media/v090/andriod_1.jpg)
[![andriod_2](/en/media/v090/andriod_2.jpg){ width="300" }](/en/media/v090/andriod_2.jpg)

💰 After testing, we will be able to offer **Yucca Enterprise** customers branded application builds, the cost is not included in the license and is discussed individually.

## New flags

* [log-level](/en/configuration/#log_level_1)
* [smtp-server-event-window-duration](/en/configuration/#event_window_duration)
* [alloc-dir-warning-threshold](/en/configuration/#alloc_dir_warning_threshold)

## Other changes

### Added

* [epic] Mosaic (#688)
* [epic] Android mobile app (BETA) (#1096)
* [server] Make AllocDirWarningThreshold setting public (#1091)
* [server] Ability to override motion detection event interval (#1073)
* [UI] Scaling the progress bar relative to the cursor (#1071)
* Night builds (#886)
* Update hls.js (#787)
* Distribution via Apt, Yum, Docker image

### Fixed

* UI / Correct the layout for the warning banner closing cross (#1114)
* [UI] Warning banner closed state not remembered (#1090)
* [build] Incorrect architecture in deb arm (#1060)
* Title in stream control tabs and admin panel (#1028)
* Unable to download screenshot on some near square resolutions (#902)
* Can't take screenshot in firefox on android (#720)

## What's next?

Installation instructions can be found in the section [Installation](/en/install/).

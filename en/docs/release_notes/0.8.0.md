---
title: What's new in Yucca 0.8.0
description: Video surveillance for home and business
---

# What's new in Yucca 0.8.0

*03.03.2023*


## Motion detection archive recording (archive on-motion)

[![archive-on-motion_2](/en/media/v080/archive-on-motion_2.png)](/en/media/v080/archive-on-motion_2.png)

This feature saves disk space, as the archive will only be written when the camera reports motion detected in the frame. We always keep a certain amount of data in memory, so in fact the recording starts a few seconds before the moment when the notification from the camera arrived.

[![archive-on-motion_1](/en/media/v080/archive-on-motion_1.png)](/en/media/v080/archive-on-motion_1.png)

## Live streams on demand (live on-demand)

The functionality will allow you to connect to the camera while watching. When exiting the view, the stream goes into a pause state. This can be useful for saving traffic on cameras with cellular-only network access or in [public streams](/en/release_notes/0.7.0/#_1) with broadcasts embedded on your site.

[![liveOnDemand](/en/media/v080/liveOnDemand.gif)](/en/media/v080/liveOnDemand.gif)

## 💰 Manage access to cameras in the user settings

!!! info "💰 Available only in Enterprise"

Now you can grant access to the camera both in the settings of the stream and the user.

[![acl_in_user](/en/media/v080/acl_in_user.png)](/en/media/v080/acl_in_user.png)

## Server and stream logs in UI

It will become easier to debug any problems with the stream or the server itself, because you no longer need to go to the console via ssh and remember the command how to view the server logs 🙃. By default, the last 50 messages are visible, but the number can be adjusted with the [log_buffer_size](/en/configuration/#log_buffer_size) option.
[![self_logs_1](/en/media/v080/self_logs_1.png)](/en/media/v080/self_logs_1.png)

P.S.:  `sudo journalctl -o short -n 50 -f -u yucca`

## 💰 Personal Tokens

!!! info "💰 Available only in Enterprise"

We [published](/en/release_notes/0.7.0/#_5) this functionality earlier, but it appeared in the interface just now. This will be useful to everyone who makes their automation, integration with billing and other interactions with the API.

[![tokens](/en/media/v080/tokens.gif)](/en/media/v080/tokens.gif)

## 💰 Direct link and autoplay option in public streams

!!! info "💰 Available only in Enterprise"

[![new_shared_stream](/en/media/v080/new_shared_stream.png)](/en/media/v080/new_shared_stream.png)

## Support old OS

We have expanded the list of supported systems to cover more older GNU/Linux releases.  
Here is a list of systems on which Yucca is now guaranteed to run:

* CentOS 5 and later
* Debian 6 and later
* Ubuntu 10.04 and later

## New flags

* [log-buffer-size](/en/configuration/#log_buffer_size)
* [log-file](/en/configuration/#log_file)
* [security-audit-logs](/en/configuration/#audit_logs)
* [self-logs](/en/configuration/#self_logs)
* [streams-archive-download-request-lifetime](/en/configuration/#archive_download_request_lifetime)
* [streams-live-on-demand-lease-duration](/en/configuration/#live_on_demand_lease_duration)
* [streams-live-on-demand-lease-interval](/en/configuration/#live_on_demand_lease_interval)

## Other changes

### Added

* Add a console command to create a preview (#1067)
* [server] Request and apply a trial license from yucca (#1062)
* Username validation when logging in to the client (#1059)
* Add port transfer support to local_stream (#1056)
* Mark executor=docker as depracated (#1034)
* Show stream status in public streams (#1021)
* Show Host ID and Server ID on license page (#1019)
* Build a docker image with a server (#1014)
* The ability to disable AutoPlay in public streams (#1012)
* Notify administrators by mail about an expiring license (#1011)
* Notify the user by mail about a password change (#1010)
* Return all configuration errors instead of the very first one (#997)
* [UI] Live on demand (#996)
* [UI] Public streams/Shared streams, link to m3u8 (#994)
* [UI] Motion detection archive recording (#976)
* Stubs in the free version on ent functionality (#975)
* Self-logs for streams(ffmpeg). UI (#974)
* [UI] Details with error is always deployed (#971)
* Brew. Remove FFmpeg and Docker installation (#969)
* Server log output to a file with rotation (#962)
* [backend] Allow h265 streams to be parked (#959)
* Self-logs for the server. UI (#953)
* Redirect to the destination page after login (#940)
* Warning if free space runs out. UI (#936)
* Cluster/distributed mode of operation. Research (#893)
* Get a list of fitch from the API (#865)
* Personal tokens. UI (#851)
* Debugging problems with the flow in the UI (stream of the parking attendant/ffmpeg log to the interface) (#811)
* When installing a light logo, it is not visible on the main page. (#803)
* Setting up access to cameras when creating/editing a user. UI (#771)
* Scaling the progress bar (#749)
* Check out the new sqlite driver (#745)
* Update enterpise elements in the free version (#608)
* Publish the basic Yucca Dashboard for Grafana (#818)
* Simplified method for stream status (#553)

### Changed

* Add UID to UserPermission (#984)
* Time limit for personal tokens (#956)
* Self-logs for the server. Backend (#952)

### Fixed

* [swagger] mixed up descriptions of archive/download and archive/download/{request_id} methods (#1066)
* [server] Incorrect file length after concatenation (#1065)
* [server] When downloading segments of more than 4 hours, mp4 files are broken (#1057)
* [server] The server freezes when trying to add more than 1 privilege in the user settings when using sqlite3 (#1055)
* [server] Stream will launch 2 ffmpeg instances when changing the StreamSource of the failed stream (#1053)
* deb packages are not installed on debian (#1051)
* [server] Incorrect ranges are given back to the future (#1050)
* Error when downloading ffmpeg in arm architectures (#1033)
* Incorrect time headers when giving static files (#1032)
* [UI] The quota for streams is not displayed correctly in the user profile (#1031)
* [server] The quota for streams is not displayed correctly in the user profile (#1030)
* [epic] The quota for streams is not displayed correctly in the user profile (#1029)
* There is no tab for issuing a personal token for a managed user (#1027)
* [Backend] Privileges in the user settings do not work (#1026)
* When a stream falls with an error, the error is written to the server log, but it is not written to the stream log. (#1020)
* Public streams via Yucca Gateway do not work (#998)
* Search for streams with the name in Russian does not work (#980)
* Return the architecture to deb packages (#973)
* A user with viewing access should not see the stream launch button (#972)
* UI. Incorrect redirect after routing update (#970)
* When you click on the slider, it disappears, repeat on mobile phones #802 (#965)
* [UI] The client does not update ranges on some zoom states (#963)
* Error when providing public access in the free version (#943)
* Error when starting the stream rpc error: no registered streams (#571)

## What's next?

Installation instructions can be found in the section [Installation](/en/install/).

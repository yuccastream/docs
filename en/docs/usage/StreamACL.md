---
title: Access Control
description: Video surveillance for home and business
---

# Access Control

!!! info "💰 Feature available with [**subscription**](https://yucca.app/en/prices) Yucca Plus or Yucca Enterprise""

## Overview

Allows you to differentiate the user's access level to a stream.  
Access levels:

- View live stream only
- View live stream + archive
- Edit stream
- Stream Administration

[![StreamACL1](/en/media/features/StreamACL/streamACL1.png)](/en/media/features/StreamACL/streamACL1.png)

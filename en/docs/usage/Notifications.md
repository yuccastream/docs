---
title: Notifications
description: Video surveillance for home and business
---

# Notifications

!!! info "💰 Feature available with [**subscription**](https://yucca.app/en/prices) Yucca Plus or Yucca Enterprise"

## SOON

The feature is still in development
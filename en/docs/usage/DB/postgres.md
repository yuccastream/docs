---
title: PostgreSQL Installation
description: Video surveillance for home and business
---

# PostgreSQL Installation

!!! info ""
    If you need assistance from a specialist to solve this or any other problem, please review our **[technical support terms](/en/support/)**.

There are several ways to install the PostgreSQL DBMS. We will describe 2 of the simplest and fastest methods according to our opinion. **You need to choose the most suitable one for you.**

!!! warning "Simple passwords are used in the examples; it is advisable to change them!"

## Installing PostgreSQL from Packages on a Host

To install, use the official instructions from the developer's website <https://www.postgresql.org/download/>.

!!! info "Quick installation in Debian or Ubuntu:"

    ```bash
    sudo apt install postgresql-common
    sudo sh /usr/share/postgresql-common/pgdg/apt.postgresql.org.sh
    sudo apt install postgresql-14 -y
    ```

After installation and startup, you need to create a database and a role for connection.

```bash
sudo -u postgres psql -c "CREATE DATABASE yucca;"
sudo -u postgres psql -c "CREATE USER postgres WITH PASSWORD 'postgres';"
sudo -u postgres psql -c 'GRANT ALL PRIVILEGES ON DATABASE yucca TO postgres;'
```

## Installing PostgreSQL in Docker

Run the official container with the necessary parameters:

```sh
docker run -d \
    --name yucca-postgres \
    --net host \
    --restart always \
    -e POSTGRES_USER=postgres \
    -e POSTGRES_PASSWORD=postgres \
    -e POSTGRES_DB=yucca \
    postgres:14.4-alpine
```

## Configuring Yucca Connection to the PostgreSQL Database

Specify the type `postgres` in the [database.type](/en/configuration/#type) parameter, as well as other connection details:

- [database.host](/en/configuration/#host)
- [database.name](/en/configuration/#name)
- [database.user](/en/configuration/#user)
- [database.password](/en/configuration/#password)

Example of the final configuration via file:

```toml
[database]
  host = "127.0.0.1:5432"
  name = "yucca"
  password = "postgres"
  type = "postgres"
  user = "postgres"
```

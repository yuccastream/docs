---
title: Work with license
description: Video surveillance for home and business
---

# Work with license

Yucca Plus/Enterprise requires a license to operate. You can buy it or release a trial one in [customer portal](https://my.yucca.app).
If you have the **Free** version installed, you need to update it to **Plus/Enterprise**, use [instructions](/en/install/). When switching from Free to Plus/Enterprise and back, all data is saved. For the license to work correctly, the server must have access to the site `https://license.yucca.app` (tcp, udp 443).

## Adding

Start the server and authorize. If the license is not activated, you will see a notification banner.
Go to the license management section using the link in the banner:

[![license_01](/en/media/other/license/en_license_01.png)](/en/media/other/license/en_license_01.png)

Specify your key and click "Add":

[![license_02](/en/media/other/license/en_license_02.png)](/en/media/other/license/en_license_02.png).

If the license is valid and valid, you will see its parameters (ID, expiration date and owner contact):

[![license_03](/en/media/other/license/en_license_03.png)](/en/media/other/license/en_license_03.png)

## Restrictions

When the license expires, the server will continue to run with the settings it had before the license expired, almost all features will be blocked.
But the server will continue to work, all running threads and the archive will continue to work.
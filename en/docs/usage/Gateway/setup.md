---
title: Gateway
description: Video surveillance for home and business
---

## Yucca Gateway Setup

## Start Yucca Gateway

To run Yucca Gateway the same Yucca binary, Enterprise version is used, be sure to specify a separate directory in `data-dir`.

!!! note "Installation instructions [Yucca](/en/install/)"

When installing via deb/rpm package manager, the `yucca-gateway` service is already installed on the system, you only need to run

``sh
sudo systemctl status yucca-gateway.service
sudo systemctl enable yucca-gateway.service
sudo systemctl start yucca-gateway.service
sudo systemctl status yucca-gateway.service
```

If you installed Yucca differently, you can start Gateway like this:  

```sh
/opt/yucca/yucca gateway --data-dir /opt/yucca/data

```

By default, Yucca Gateway listens on port ``9920``, but you can override this behavior. For a full list of options, see the `/opt/yucca/yucca gateway --help` tooltip

## Receive token

To connect Yucca Gateway to Yucca Server, it will need an end server administrator token. A personal token can be issued in the user profile, under [tokens](/en/usage/PersonalTokens/).

## Connect Yucca Gateway to Yucca Server

For correct operation, you need to ensure network availability between Yucca Gateway and all Yucca Servers.  

Pre-install ``curl`` if you do not have it.

``sh
sudo apt update && sudo apt install curl -y
```

All the following actions can be performed from any host, as long as you have network accessibility.

Example command to connect:

```sh
curl -s -d '{"address": "http://localhost:9910", "token": "p.f22003974f22e41a2c48dc58ac25c24f412bff151"}' http://127.0.0.1:9920/v1/system/gateway/server
```

- `address` is the address of the Yucca Server at which the Yucca Gateway will access it.
- ``token` - respectively the token we received earlier.
- `http://127.0.0.1:9920/v1/system/gateway/server` - the address of Yucca Gateway, to which we send the request.

An example of a correct response:

```json
{
  "active": true,
  { "address": "http://localhost:9910",
  "healthy": true,
  "id": 0,
  "name": ""
  "verified": true
}
```

You can add multiple Yucca Server nodes in this way.

Everything is ready!  
Now when authorizing through Yucca Gateway at `http://127.0.0.1:9920`, we will get to the Yucca Server, where there is a user with the credentials of which we authorize.

Translated with DeepL.com (free version)
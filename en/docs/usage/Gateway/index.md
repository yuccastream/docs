---
title: Gateway
description: Video surveillance for home and business
---

# Yucca Gateway

!!! info "The feature is free and does not require a license"

!!! warning "BETA"
    The functionality is still being tested; later, the format of operation and monetization may change.

## Description

Yucca Gateway is a special operating mode in the form of a gateway, allowing you to create a unified entry point (authentication) for clients from different installations of Yucca Server.
You can have one internet address and several servers, each with a different set of users.
For example, you are an internet service provider with presence zones in different cities and a separate installation of Yucca in each city. Each of your servers has its own clients, but within the framework of an advertising campaign, you want to have a single entry point for all clients.

The diagram below describes the approximate logic of operation:

![yucca_gw](/en/media/v070/yucca_gw.png)

## Requirements and Limitations

- User names (logins) from different servers **should not intersect**; otherwise, the client may end up on a random server where authorization was successful.
- Only the Enterprise version of Yucca Server can be connected to Yucca Gateway; working with free versions is not supported at the moment.

---
title: Changing path for storing the archive
description: Видеонаблюдение для дома и бизнеса
---

# Changing path for storing the archive

By default the archive with cameras is saved in `/opt/yucca/data/alloc/` directory, the path is defined by [alloc_dir](/en/configuration/#alloc_dir) parameter.
If you want to change this behavior, create a directory in a new path and apply permissions to it for the user under which Yucca is run, the default is the user `yucca` for example:

The directory names and paths are for example.

```sh
sudo mkdir -p /mnt/external_drive_8TB/yucca_alloc
sudo chown yucca:yucca /mnt/external_drive_8TB/yucca_alloc
```

!!! info "Disk mount"
    If you have mounted a separate disk, make sure it shows up in `df -h`, also don't forget to add a mount entry to `/etc/fstab` so that the disk is mounted when the server reboots.

Stop the Yucca server:

```sh
sudo systemctl stop yucca
```

Replace the parameter in [alloc_dir](/en/configuration/#alloc_dir) in the configuration:

=== "Configuration file"

    ```sh
    alloc_dir = "/mnt/external_drive_8TB/yucca_alloc"
    ```

=== "Environment variable"

    ```sh
    YUCCA_SERVER_ALLOC_DIR=/mnt/external_drive_8TB/yucca_alloc
    ```

=== "Command line argument"

    ```sh
    --alloc-dir=/mnt/external_drive_8TB/yucca_alloc
    ```

Start the Yucca server:

```sh
sudo systemctl start yucca
```

Verify that files are created in the new directory and the web interface shows the new path in the configuration tab.

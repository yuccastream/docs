---
title: Tunnel
description: Video surveillance for home and business
---

# Tunnel

!!! info "💰 Feature available by [**subscription**](https://yucca.app/en/prices) Yucca Plus"

## Overview

The feature allows you to access your installation on a private network without a public IP address.
Yucca connects to our infrastructure and through it you access the NVR.

The scheme of work looks like this:

[![schema1](/en/media/features/Tunnel/schema1.png)](/en/media/features/Tunnel/schema1.png)

## Example of work

<video loop autoplay muted>
  <source src="/en/media/features/Tunnel/tunnel1.mp4" type="video/mp4">
</video>

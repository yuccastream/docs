---
title: Quotas
description: Video surveillance for home and business
---

# Quotas

!!! info "💰 Feature available with [**subscription**](https://yucca.app/en/prices) Yucca Plus or Yucca Enterprise""

## Overview

Quotas allow you to limit a user in the number of threads they can add and concurrent sessions.
It is also possible to limit [globally](/en/configuration/#quota) per server the number of threads, users and other resources.  

[![quota1](/en/media/features/Quota/quota1.png)](/en/media/features/Quota/quota1.png)

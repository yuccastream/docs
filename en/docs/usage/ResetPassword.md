---
title: Password Reset
description: Video surveillance for home and business
---

## Reset password

## Mail Confirmation

In Yucca 0.10.0 it is now possible to reset password via mail confirmation.
To do this, the user must have a real E-mail and a configured [mail client](/en/configuration/#smtp_client) in the server.

[![ResetPassword1](/en/media/other/ResetPassword/ResetPassword1.png)](/en/media/other/ResetPassword/ResetPassword1.png))

## Command line

If the mail client is not configured, you can reset the password of any user, including the administrator, at the command line:

```bash
/opt/yucca/yucca admin change-password --config=/opt/yucca/yucca.toml --password <NEW_PASSWDORD>
```

If you use the configuration not through the file, then specify your variant of connection to the database. 
You can see the connection parameters in the tooltip:

```bash
/opt/yucca/yucca admin change-password --help
Change a user's password

Usage:
  yucca admin change-password [flags]

Flags:
      --config string Path to configuration file (default "/opt/yucca/yucca/yucca.toml")
      --data-dir string The data directory used to store state and other persistent data (default "data")
      --database-busy-timeout int Timeout in seconds for waiting for an SQLite table to become unlocked (default 500)
      --database-ca-cert-path string The path to the CA certificate to use (default "/etc/ssl/certs")
      --database-cache-mode string Shared cache setting used for connecting to the database (private, shared) (default "shared")
      --database-client-cert-path string The path to the client cert
      --database-client-key-path string The path to the client key
      --database-conn-max-lifetime duration Sets the maximum amount of time a connection may be reused (default 0s)
      --database-host string Database host (not applicable for sqlite3) (default "127.0.0.1:5432")
      --database-max-idle-conn int The maximum number of connections in the idle connection pool (default 2)
      --database-max-open-conn int The maximum number of open connections to the database
      --database-name string The name of the Yucca database (default "yucca")
      --database-password string The database user's password (not applicable for sqlite3) (default "postgres")
      --database-path string SQLite database location
      --database-ssl-mode string SSL mode for Postgres (disable, require or verify-full) (default "disable")
      --database-user string The database user (not applicable for sqlite3) (default "postgres")
  -h, --help Help for change-password
      --name string User name (leave blank to update the password for the first user)
      --password string User password
      --state-dir string The directory used to store state and other persistent data (default "<data-dir>/state")

```

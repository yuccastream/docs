---
title: Receive events by Email
description: Video surveillance for home and business
---

# Receive Events by Email

!!! info "Supported since version 0.6.0"

Since version 0.6.0 it is now possible to receive events from cameras via email.

## How it works

Most modern IP cameras can detect motion in the frame and send an email notification about it.
In our case [Yucca](https://yucca.app/) acts as an [SMTP](https://en.wikipedia.org/wiki/SMTP) server, listening on a dedicated port. At startup, each stream is assigned a unique email address. The camera settings then specify Yucca as the SMTP server and the unique assigned address as the sender and destination address. When the camera detects motion in the frame, it sends a message about it to the server. The server, for its part, identifies which thread the notification belongs to and records the time when it arrived.

[![how_work](/en/media/other/smtp/how_work.png)](/en/media/other/smtp/how_work.png)

Based on this data, an event is created, which is then displayed on the timeline in the player's progress bar.

[![event_labels](/en/media/v060/event_labels.png)](/en/media/v060/event_labels.png)

The duration of an event is defined by [event_window_duration](/en/configuration/#event_window_duration)

## How to customize

To work in the settings you need to enable [SMTP server](/en/configuration/#smtp_server), by default it is already enabled and listens on port 1025.
To exclude unauthorized connections set [username](/en/configuration/#username) and [password](/en/configuration/#password_1), by default they are not set.

Further in the settings of the already added camera you can see the requisites that will be useful later in the configuration of the cameras themselves.

[![yucca_01](/en/media/other/smtp/yucca_01.png)](/en/media/other/smtp/yucca_01.png)
[![yucca_02](/en/media/other/smtp/yucca_02.png)](/en/media/other/smtp/yucca_02.png)

The *Server* field can be overridden with the [domain](/en/configuration/#domain) flag. By default, the first IP on the interface with the default route is displayed.

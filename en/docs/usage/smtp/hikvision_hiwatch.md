---
title: Configuring Motion Detection by email on Hikvision / HiWatch cameras
description: Video surveillance for home and business
---

# Configuring Motion Detection by email on Hikvision / HiWatch cameras

Open the web-interface of the camera with administrator rights.

Go to **Configuration > Network > Advanced Settings > Email**

Enter connection details - *SMTP Server*, *SMTP Port*, *Sender's Address* and add *Receiver*. The last 2 must match. If authorization is enabled in SMTP server settings, enter *username* and *password*, also enable Authentication.

Press **Test**. If everything is correct, you’ll see a message `Succeed` and in the logs of Yucca message about created event:

```log
Event annotation created, stream_id: 10
```

And a yellow marker will appear in the camera’s progress bar.

=== "English UI"

    [![hik_01](/en/media/other/smtp/hik_01.png)](/en/media/other/smtp/hik_01.png)

=== "Russian UI"

    [![hik_ru_01](/en/media/other/smtp/hik_ru_01.png)](/en/media/other/smtp/hik_ru_01.png)

[![bar_event](/en/media/other/smtp/bar_event.png)](/en/media/other/smtp/bar_event.png)

---

Next, you need to enable Motion Detection. Go to **Configuration > Event > Basic Event > Motion Detection** and turn on *Enable Motion Detection* function. In the tab called **Linkage method** turn on *Send Email* as a type of notification.

=== "English UI"

    [![hik_02](/en/media/other/smtp/hik_02.png)](/en/media/other/smtp/hik_02.png)

=== "Russian UI"

    [![hik_ru_02](/en/media/other/smtp/hik_ru_02.png)](/en/media/other/smtp/hik_ru_02.png)

---
title: Configuring Motion Detection by email on XM cameras
description: Video surveillance for home and business
---

# Configuring Motion Detection by email on XM cameras

!!! info "Cameras from this manufacturer are often sold under different brands, but they are easily recognized by their web interface, which only works in Internet Explorer."

Open the web-interface of the camera with administrator rights.

Go to **DeviceCfg (Setting) > System > NetService**

[![xm_01](/en/media/other/smtp/xm_01.png)](/en/media/other/smtp/xm_01.png)

---

Double-click on **EMAIL** field , SMTP settings field will open

[![xm_02](/en/media/other/smtp/xm_02.png)](/en/media/other/smtp/xm_02.png)

---

Enter connection details - *SMTP server*, *port*, *sender* and *receiver*. The last 2 must match. If authorization is enabled in SMTP server settings, enter *username* and *password*.

Press **Mail Testing**. If everything is correct, you’ll see a message `Test message sent success` and in the logs of Yucca message about created event:

```log
Event annotation created, stream_id: 10
```

And a yellow marker will appear in the camera’s progress bar.

[![xm_03](/en/media/other/smtp/xm_03.png)](/en/media/other/smtp/xm_03.png)
[![bar_event](/en/media/other/smtp/bar_event.png)](/en/media/other/smtp/bar_event.png)

---

Next, you need to enable motion detection. Go to **DeviceCfg (Setting) > Alarm > Video Motion**

[![xm_04](/en/media/other/smtp/xm_04.png)](/en/media/other/smtp/xm_04.png)

---

And turn on *enable* option and *Send Email* as a type of notification.

[![xm_05](/en/media/other/smtp/xm_05.png)](/en/media/other/smtp/xm_05.png)

---

In the same tab by clicking *Setting* button, you can set visibility area of Motion Detection

[![xm_06](/en/media/other/smtp/xm_06.png)](/en/media/other/smtp/xm_06.png)

---
title: Cluster
description: Video surveillance for home and business
---

# Cluster (Distributed Mode)

!!! info "💰 Function available with [**subscription**](https://yucca.app/en/prices) to Yucca Enterprise"

## Overview

The cluster function allows placing cameras on different servers while having a unified user database.
This is an opportunity to scale to thousands of cameras and efficiently utilize all your physical servers and data storage systems.
All Yucca servers in the cluster, being its participants, allow users to access the interface of any server and access cameras on any server transparently, without the need for a special entry point or specific settings.
Although working with a load balancer is not excluded and even recommended, as it helps to direct traffic reliably to the server that is currently available and operational.

The operation scheme looks something like this:

[![yuccaCluster](/en/media/features/Cluster/yuccaCluster.drawio.png)](/en/media/features/Cluster/yuccaCluster.drawio.png)

## Requirements and Limitations

- In cluster mode, only [PostgreSQL](/en/usage/DB/postgres/) is supported.
- Currently, there is no migration mechanism from single-server installations to cluster mode. We are working on this toolkit and will update the documentation when it becomes available.

---
title: API tokens
description: Video surveillance for home and business
---

# API tokens

!!! info "💰 Feature available by [**subscription**](https://yucca.app/en/prices) Yucca Plus or Yucca Enterprise""

## Overview

We provide a programmatic interface to all Yucca server resources. API documentation is available in SwaggerUI format on any installation at [http://<your address>/ui/swagger/](https://demo.yucca.app/ui/swagger/).
You can authorize with a token by passing it in the `X-Yucca-Token` header, the `token` cookie, or as an argument to a `token` request.
With a personalized API token, you can manage any resources that are available to your user, except content (watching videos) and UI.

## Quick Start.

Go to the user profile, **Tokens** tab and create a token for the desired period or indefinitely. Save the token, you will not be able to view it again.  

[![PersonalTokens](/en/media/features/PersonalTokens/PersonalTokens.png)](/en/media/features/PersonalTokens/PersonalTokens.png))

## Example with cURL

```bash
curl -X 'GET' \.
  ``https://demo.yucca.app/v1/annotations?all=true'\
  -H 'accept: application/json' \.
  -H 'X-Yucca-Token: p.bb42c69e83915664cae70e442a349eab509890d21'
```

## SwaggerUI Example

<video loop autoplay muted>
  <source src="/en/media/features/PersonalTokens/PersonalTokens.mp4" type="video/mp4">
</video>

---
title: Hardware Selection
description: Video surveillance for home and business
---

# Hardware Selection

*29.12.2022*

## Introduction

**How to run 200+ cameras on the server for 300 euro?**

[![grafana_node](/en/media/note/hw_usage/grafana_node.png)](/en/media/note/hw_usage/grafana_node.png)

We are often asked, how many cameras can I add to Yucca on a server with my hardware resources? The question is always individual and it is impossible to answer it in a template for everyone. A lot depends on the type of cameras, bitrate, processor and other variables. Therefore, we decided to collect some statistics from the server of one of our clients, with screenshots and detailed explanations, so that everyone could imagine how many resources are needed in his particular case...

We published similar statistics about the launch on [**Raspberry Pi 4**](/en/note/run_on_rpi4/)

## Our installation

And so Yucca version is installed ==0.7.2== , and connected ==154 cameras==, archive depth is almost 4 years, the bitrate of the stream from the cameras is around 1M (megabits per second). In addition to Yucca, postgresql is installed on the same server, actually as a database.

[![ycc_main](/en/media/note/hw_usage/ycc_main.png)](/en/media/note/hw_usage/ycc_main.png)
[![ycc_admin](/en/media/note/hw_usage/ycc_admin.png)](/en/media/note/hw_usage/ycc_admin.png)
[![grafana_ycc](/en/media/note/hw_usage/grafana_ycc.png)](/en/media/note/hw_usage/grafana_ycc.png)
(If you want the same dashboard, the instructions are available at [link](https://gitlab.com/yuccastream/public/monitoring ))

What resources are used for all this and how many of them are disposed of?

## Motherboard (Motherboard)

[**Z8NR-D12**](https://www.asus.com/en/Commercial-Servers-Workstations/Z8NRD12 /) - this is a very old chassis, you can buy this on the secondary market for about 200 euro.

[![motherboard](/en/media/note/hw_usage/motherboard.png)](/en/media/note/hw_usage/motherboard.png)
*screenshot from the site of one of the spots known to us*

In this version, perhaps, there are not enough 2 power supplies, but you can look for similar chassis, for example from Supermicro.

## Processor (CPU)

[**Intel(R) Xeon(R) CPU X5690 @ 3.47GHz**](https://www.intel.com/content/www/us/en/products/sku/52576/intel-xeon-processor-x5690-12m-cache-3-46-ghz-6-40-gts-intel-qpi/specifications.html ) - 2 processors with 12 threads, the total is 24. On Avito, you can buy one for 30 euro apiece.

CPU is one of the most important resources, it is in it that you will always rest when the number of threads increases. After all, the more threads, the more processes and the more processor time we need. Accordingly, the most attention should be paid not even to recycling, as in 1 screenshot, but to LA or Load Average (if you don't know what it is, then read [тут](https://www.google.com/search?q=%D1%87%D1%82%D0%BE+%D1%82%D0%B0%D0%BA%D0%BE%D0%B5+load+average+linux&oq=Xnj+nfrjt+Load+Avarage)) 2 screenshot.

And so let's look at the disposal in 24 hours, there are peaks here, but the average utilization is around 30%
[![cpu_util](/en/media/note/hw_usage/cpu_util.png)](/en/media/note/hw_usage/cpu_util.png)

On the LA chart for the day, the situation is similar, around 30% - 40%
[![cpu_la](/en/media/note/hw_usage/cpu_la.png)](/en/media/note/hw_usage/cpu_la.png)

Remember that this is an indicator for 150 cameras, which means that the server is about half full and in theory you can add the same number of cameras, provided that the cameras have a similar bitrate.

We do not recommend adding cameras to servers where the LA is already above one. With LA > 1, this means that the queue will grow all the time and maintenance problems and degradation are almost inevitable, you need either more processor or 1 more Yucca server. Therefore, with a large number of clients and cameras, first of all, always draw your installations on monitoring and monitor the indicators.

## Random Access Memory (RAM)

**9965447-034.A00LF** - server ECC memory for Avito about 10 euro for a fee.

In our case, 16 gigabytes (8 per processor socket) is enough with a 3rd margin and is disposed of around 4 gigabytes.

[![grafana_ram](/en/media/note/hw_usage/grafana_ram.png)](/en/media/note/hw_usage/grafana_ram.png)

## Disk

Disk array is a topic that requires a separate note on how to do and how not to do, I'll tell you how to calculate the predicted volume manually, but you can always find an online calculator for your bitrate and array type.

So, at this stand, our client places only public cameras and the unexpected loss of the archive from these cameras is not critical to him. Therefore, if your archive is of great value, then **do not do so** as described here.

**ST10000VN0004-1Z** - Seagate IronWolf 10Tb NAS.  
4 pieces in the **Raid0** configuration give ==37 terabytes==, the cost is around 200 euro per piece.

```sh
$ df -h | grep yucca
/dev/md0            37T          15T   20T           43% /opt/yucca/data/alloc

$ cat /etc/mdadm/mdadm.conf 
DEVICE partitions
ARRAY /dev/md0 level=raid0 num-devices=4 metadata=1.2 name=yucca:0 UUID=xxx

```

Of course, due to the fact that this is Raid0, we do not experience any performance problems at all, when recording an archive in parallel with 150 cameras, and there will be no problems with 300, but also fault tolerance is at zero, so everyone chooses a solution for himself.

### How to calculate how much space is needed for the archive

Suppose we have 150 identical cameras with a stream bitrate of 2M, from them we want to write an archive with a depth of 14 days:

- 2 megabits per second, that's 2/8 = 0.25 megabytes
- 0.25 * 60 * 60 = 900 megabytes per hour
- 900 * 24 = 21600 megabytes => 21.6 gigabytes per day with 1 camera
- 21.6 * 14(days) = 302.4 gigabytes in 14 days from 1 camera
- 302.4 * 150 = 45360 gigabytes => 45.36 terabytes

Such a simple calculation allows you to calculate approximately how much our archive will take up. It is also worth considering that arrays should not be filled to the brim in 99% and at least 5-10% is better to keep free.

## Conclusions

So, we have *chassis + processors + RAM* for about 300 euro and a disk array for 400 euro. In total, about 700 euro for a full-fledged working solution with the ability to sell and provide services to customers with a capacity of 300 cameras.  

*Let's dream a little ...
A license for Yucca costs 60 euro a month and we do not have limits on cameras, if you have a modern chassis and 10,000 cameras, we do not limit you :) The average tariff for a camera with an archive of 14 days from providers now costs ~ 5-7 euro a month, that is, 1500 euro a month can be earned on such a server without much expense with a payback of 1 month, though if you already have 300 ready-made clients* 🙂

Thanks to everyone who read to the end!  
You can ask a question through the feedback forms on [website](https://yucca.app/ru#contact) by writing to the mail [info@yucca.app](mailto:info@yucca.app) or in [Telegram chat](https://t.me/yuccastream) communities.
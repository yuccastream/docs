---
title: System requirements
description: Video surveillance for home and business
---

# System requirements

To launch Yucca, the following resources are enough:

1. CPU - 1 core
2. RAM - 512Mb.
3. DISK (free space) - 1Gb.

These are the minimum requirements for running the server, what will be the consumption when adding cameras, read [here](/en/usage/SystemRequirements/HardwareSelection/) and [here](/en/usage/SystemRequirements/RunOnRaspberryPi4/).

## Camera requirements

The camera must stream via **RTSP** protocol, video must be in **h264** codec, support for **h265/hevc** is planned later.

## Maximum server capacity

From the experience of our customers, we have come to the conclusion that on average the ideal ratio is **10 cameras per 1 CPU core**.
So, suppose on a server with 24 cores and a processor frequency of *2.5-3.0 Ghz*, 240-250 cameras can be parked without problems.
At large values, [LA](https://www.google.com/search?q=What+Is+load+average+linux) starts to grow strongly and artifacts and instability may be observed.

## Disk space calculation

Suppose we have 150 identical cameras with a stream bitrate of 2M, we want to write an archive from them 14 days deep:

- 2 megabits per second, that's 2/8 = 0.25 megabytes
- 0.25 * 60 * 60 = 900 megabytes per hour
- 900 * 24 = 21600 megabytes => 21.6 gigabytes per day from 1 camera
- 21.6 * 14(days) = 302.4 gigabytes in 14 days from 1 camera
- 302.4 * 150 = 45360 gigabytes => 45.36 terabytes

Here is such a simple calculation allows you to roughly calculate how much our archive will occupy. It is also worth considering that arrays should not be filled to capacity at 99%, and it is better to keep at least 5-10% free.

---
title: Users
description: Video surveillance for home and business
---

# Users

!!! info "💰 Feature available with [**subscription**](https://yucca.app/en/prices) Yucca Plus or Yucca Enterprise""

## Overview

By default, Yucca has only 1 user, the administrator. This feature allows you to create additional accounts.

[![users1](/en/media/features/Users/users1.png)](/en/media/features/Users/users1.png)

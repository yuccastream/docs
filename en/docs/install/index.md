---
title: Installation
description: Video surveillance for home and business
---

# Installation

## Overview

Review the following installation documents:

* [Docker](/en/install/Docker/)
* [Debian/Ubuntu](/en/install/Debian_Ubuntu/)
* [SUSE](/en/install/SUSE/)
* [RHEL/CentOS](/en/install/RHEL_CentOS/)
* [Other Linux](/en/install/other_linux/)
* [macOS](/en/install/macOS/)

## Additional

* [System requirements](/en/usage/SystemRequirements/)
* [Configuration](/en/configuration/)

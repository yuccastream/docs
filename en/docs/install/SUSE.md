---
title: Installation for SUSE
description: Video surveillance for home and business
---

# Installation for SUSE

!!! info "Support"
    If you need expert help with this or any other issue, please see our terms and conditions **[technical support](/en/support/)**.

!!! note "Use PostgreSQL, with a large number of cameras"
    When operating in production environments with a large number of cameras
    and motion detection enabled, we recommend using PostgreSQL as a more productive database.
    How to install and configure usage, read [here](/en/usage/DB/postgres/)

??? question "Which version should I choose?"
    There are 2 editions of Yucca

    === "Free"
         The completely free version does not require a license and does not contain advanced features.

    === "Plus/Enterprise"
         Contains advanced features and requires the purchase and use of a license.  
         If you have a **Plus** or **Enterprise** license, install yucca with the `ent` postfix.

Add repository

```sh 
sudo zypper addrepo -G -f https://repo.yucca.app/yum/ yucca
```

Install Free or Ent version of Yucca

=== "Free"

    ```sh
    sudo zypper install yucca
    ```

=== "Plus/Enterprise"

    ```sh
    sudo zypper install yucca-ent
    ```

After launch, the Web interface will be available at http://ip-your-server:9910

## Additional

* [System requirements](/en/usage/SystemRequirements/)
* [Configuration](/en/configuration/)

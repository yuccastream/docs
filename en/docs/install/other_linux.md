---
title: Manual installation for other Linux
description: Video surveillance for home and business
---

# Manual installation for other Linux

!!! info "Support"
    If you need expert help with this or any other issue, please see our terms and conditions **[technical support](/en/support/)**.

!!! note "Use PostgreSQL, with a large number of cameras"
    When operating in production environments with a large number of cameras
    and motion detection enabled, we recommend using PostgreSQL as a more productive database.
    How to install and configure usage, read [here](/en/usage/DB/postgres/)

??? question "Which version should I choose?"
    There are 2 editions of Yucca

    === "Free"
         The completely free version does not require a license and does not contain advanced features.

    === "Plus/Enterprise"
         Contains advanced features and requires the purchase and use of a license.  
         If you have a **Plus** or **Enterprise** license, install yucca with the `ent` postfix.

## Manual installation

Run `arch` to find out your architecture

| Output of `arch` | Designation in the documentation|
|:---:|:---:|
| x86_64 | amd64 |
| x86 | 386 |
| armv6l | armv6 |
| armv7l | armv7 |
| aarch64 | arm64 |

Create a directory for installation and go to it, download the latest version of Yucca:

=== "Free"
    === "amd64"

        ```bash
        (
        sudo mkdir -p /opt/yucca/ffmpeg
        cd /opt/yucca
        sudo wget https://releases.yucca.app/latest/yucca_linux_amd64.tar.gz
        sudo tar -xzvf yucca_linux_amd64.tar.gz
        sudo rm -f yucca_linux_amd64.tar.gz
        )
        ```

    === "arm"

        ```bash
        (
        sudo mkdir -p /opt/yucca/ffmpeg
        cd /opt/yucca
        sudo wget https://releases.yucca.app/latest/yucca_linux_arm.tar.gz
        sudo tar -xzvf yucca_linux_arm.tar.gz
        sudo rm -f yucca_linux_arm.tar.gz
        )
        ```

    === "arm64"

        ```bash
        (
        sudo mkdir -p /opt/yucca/ffmpeg
        cd /opt/yucca
        sudo wget https://releases.yucca.app/latest/yucca_linux_arm64.tar.gz
        sudo tar -xzvf yucca_linux_arm64.tar.gz
        sudo rm -f yucca_linux_arm64.tar.gz
        )
        ```

=== "Plus/Enterprise"
    === "amd64"

        ```bash
        (
        sudo mkdir -p /opt/yucca/ffmpeg
        cd /opt/yucca
        sudo wget https://releases.yucca.app/latest/yucca-ent_linux_amd64.tar.gz
        sudo tar -xzvf yucca-ent_linux_amd64.tar.gz
        sudo rm -f yucca-ent_linux_amd64.tar.gz
        )
        ```

    === "arm"

        ```bash
        (
        sudo mkdir -p /opt/yucca/ffmpeg
        cd /opt/yucca
        sudo wget https://releases.yucca.app/latest/yucca-ent_linux_arm.tar.gz
        sudo tar -xzvf yucca-ent_linux_arm.tar.gz
        sudo rm -f yucca-ent_linux_arm.tar.gz
        )
        ```

    === "arm64"

        ```bash
        (
        sudo mkdir -p /opt/yucca/ffmpeg
        cd /opt/yucca
        sudo wget https://releases.yucca.app/latest/yucca-ent_linux_arm64.tar.gz
        sudo tar -xzvf yucca-ent_linux_arm64.tar.gz
        sudo rm -f yucca-ent_linux_arm64.tar.gz
        )
        ```

<a href="https://docs.yucca.app/releases" target="_blank">Full list of available versions</a>

Generating a configuration file with default values:

```sh
sudo /opt/yucca/yucca server --config emtpy --show-config | sed 's|data_dir = ""|data_dir = "/opt/yucca/data"|' >/opt/yucca/yucca.toml
```

Create a user and set the owner and access rights:

```sh
(
sudo groupadd --gid 642 --force yucca
sudo useradd --system -u 642 --no-create-home --shell /bin/false --home-dir /opt/yucca --gid yucca yucca
sudo chown -R yucca:yucca /opt/yucca
sudo chmod -R 2775 /opt/yucca
)
```

Optional you can specify your path to save the archive by defining the parameter [alloc_dir](/en/configuration/#alloc_dir), first create the desired directory, example:

!!! info "In your case, the path may be different!"

    ```sh
    (
    sudo mkdir -p /volume1/yucca/alloc_dir
    sudo chown -R yucca:yucca /volume1/yucca/alloc_dir
    sudo chmod -R 2775 /volume1/yucca/alloc_dir
    sed -i 's|alloc_dir = ""|alloc_dir = "/volume1/yucca/alloc_dir"|' /opt/yucca/yucca.toml
    )
    ```

    Make sure everything is correct:

    ```sh
    cat /opt/yucca/yucca.toml
    ```

## Systemd autorun

Creating a systemd unit file:

```bash
sudo tee /lib/systemd/system/yucca.service <<EOF
[Unit]
Description=Yucca https://yucca.app
Documentation=https://docs.yucca.app
After=syslog.target network.target remote-fs.target nss-lookup.target

[Service]
Type=simple
User=yucca
Group=yucca
SyslogIdentifier=yucca
PIDFile=/run/yucca.pid
LimitNOFILE=65535
WorkingDirectory=/opt/yucca
ExecStart=/opt/yucca/yucca server --config /opt/yucca/yucca.toml
ExecStop=/bin/kill -s SIGTERM $MAINPID
Restart=on-failure
RestartSec=10s

[Install]
WantedBy=multi-user.target
EOF

```

Now you can start the server and check the operation:

```bash
sudo systemctl daemon-reload
sudo systemctl enable yucca
sudo systemctl start yucca
```

## Upstart autorun

!!! example ""
    For example, used in Synology NAS

Creating an autorun script Upstart init:

```sh
sudo tee /etc/init/yucca.conf <<EOF
description "Yucca https://yucca.app"
author "Yucca"

start on filesystem or runlevel [2345]
stop on runlevel [!2345]

respawn
respawn limit 10 10
setuid yucca
setgid yucca
chdir /opt/yucca
limit nofile 65535 65535
script
    exec /opt/yucca/yucca server --config /opt/yucca/yucca.toml
end script

EOF
```

Update the Upstart configuration, check, run:

```sh
sudo initctl reload-configuration
sudo initctl list | grep yucca
sudo initctl start yucca
```

## OpenRC/sysvinit autorun

There is no instruction, you can offer it.

## Checking the work

After launching, the Web interface will be available at <http://ip-your-server:9910 >

## Additional

* [System requirements](/en/usage/SystemRequirements/)
* [Configuration](/en/configuration/)

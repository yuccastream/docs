---
title: Installation for macOS
description: Video surveillance for home and business
---

# Installation for macOS

!!! info "Support"
    If you need expert help with this or any other issue, please see our terms and conditions **[technical support](/en/support/)**.

!!! notice "Yucca works on new ARM-processors Apple M1"
    On November 10, 2020, Apple [announced](https://www.apple.com/mac/m1/) the release of devices based on the new processor and for backward compatibility emulator was introduced the [Rosetta 2](https://developer.apple.com/documentation/apple_silicon/about_the_rosetta_translation_environment).

    We have not tested Yucca on new processors and do not guarantee correct operation through the emulator.

!!! note "Dependencies"
    1. Install [Homebrew](https://brew.sh/)
    2. Install `wget`

    ```shell
    (
    brew install wget
    )
    ```

??? question "Which version should I choose?"
    There are 2 editions of Yucca

    === "Free"
         The completely free version does not require a license and does not contain advanced features.

    === "Plus/Enterprise"
         Contains advanced features and requires the purchase and use of a license.  
         If you have a **Plus** or **Enterprise** license, install yucca with the `ent` postfix.

## Installing with Homebrew

Open a terminal, run the commands:

```shell
brew tap yuccastream/tap
brew install yucca
```

## Install without Homebrew

Open a terminal, download the necessary files and start the server:

=== "Free"

    ```shell
    (
    mkdir -p ./yucca
    cd ./yucca
    wget https://releases.yucca.app/latest/yucca_darwin_amd64.tar.gz
    tar -xzvf yucca_darwin_amd64.tar.gz
    rm -f yucca_darwin_amd64.tar.gz
    chmod +x ./yucca
    ./yucca server
    )
    ```

=== "Plus/Enterprise"

    ```shell
    (
    mkdir -p ./yucca
    cd ./yucca
    wget https://releases.yucca.app/latest/yucca-ent_darwin_amd64.tar.gz
    tar -xzvf yucca-ent_darwin_amd64.tar.gz
    rm -f yucca-ent_darwin_amd64.tar.gz
    chmod +x ./yucca
    ./yucca server
    )
    ```

After launch the Web interface will be available at http://ip-your-server:9910

## Additional

* [System requirements](/en/usage/SystemRequirements/)
* [Configuration](/en/configuration/)

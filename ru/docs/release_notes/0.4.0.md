---
title: Что нового в Yucca 0.4.0
description: Видеонаблюдение для дома и бизнеса
---

# Что нового в Yucca 0.4.0

*19.11.2020*

## Требование наличия FFmpeg

Изменения, необходимые в реализации новой страницы добавления стрима, требуют наличие установленного пакета [FFmpeg](https://ffmpeg.org), чтобы появилась возможность получить информацию об источнике до его добавления на сервер.

Для пользователей macOS пакет FFmpeg может быть установлен с помощью [Homebrew](http://brew.sh):

```shell
brew install ffmpeg
```

Для пользователей Linux-дистрибутивов нужно обратиться к обновленной [странице установки](/ru/install/Debian_Ubuntu/) за подробной информацией.

## Новая страница добавления стрима

Взамен простой строке ввода адреса мы добавили получение подробной информации об источнике и отображение превью:

![Новая форма добавления стрима](/ru/media/v040/stream-add-form.png)

В случае недоступности источника – информативное сообщение:

![Ошибка добавления](/ru/media/v040/stream-add-form-error.png)

## Интерфейс работы с лицензией

Добавлять и отслеживать состояние лицензии стало проще:

![Просмотр лицензии](/ru/media/v040/license.png)

## Глобальная квота на стримы

В конфигурацию сервера добавлен новый блок – [квота](/ru/configuration/old/0.4.0/#quota), можно ограничить максимальное количество пользователей или стримов на сервере, после достижения лимита пользователь получит ошибку.

Текущее число пользователей и стримов, а также установленную квоту можно увидеть на странице статистики:

![Квота](/ru/media/v040/quota.png)

## Украинский язык

Не можем обойти и это событие, теперь в интерфейсе доступен украинский язык:

![UK](/ru/media/v040/uk.png)

Если вы заметите какие-то неточности в переводе - напишите нам [на почту](mailto:info@yucca.app) или в [Telegram-чат](https://t.me/yuccastream).

## Остальные изменения

### Добавлено

* Обработка ошибки License Expired ([#603](https://gitlab.com/yuccastream/yucca/-/issues/603))
* Отдавать в метриках время истечения лицензии ([#532](https://gitlab.com/yuccastream/yucca/-/issues/532))
* Ошибки стрима для пользователя ([#535](https://gitlab.com/yuccastream/yucca/-/issues/535))
* Уровень логирования для воркера ([#544](https://gitlab.com/yuccastream/yucca/-/issues/544))

### Изменено

* Вынести worker в отдельное приложение ([#555](https://gitlab.com/yuccastream/yucca/-/issues/555))
* Изменить bind ip по умолчанию с 127.0.0.1 на 0.0.0.0 ([#600](https://gitlab.com/yuccastream/yucca/-/issues/600))
* Изменить путь к конфигурационному файлу по умолчанию ([#595](https://gitlab.com/yuccastream/yucca/-/issues/595))
* Обновление ffmpeg c 4.2.2 до 4.3.1 ([#620](https://gitlab.com/yuccastream/yucca/-/issues/620))
* После триального периода переключать сервер в ReadOnly ([#587](https://gitlab.com/yuccastream/yucca/-/issues/587))
* Пробер должен возвращать только медиа стримы ([#599](https://gitlab.com/yuccastream/yucca/-/issues/599))
* Разделить HTTP клиент с Interceptors и без ([#601](https://gitlab.com/yuccastream/yucca/-/issues/601))
* Увеличить Timeout на методе добавления стрима ([#619](https://gitlab.com/yuccastream/yucca/-/issues/619))

### Исправлено

* UI не обрабатывает ошибки при истекшей лицензии ([#606](https://gitlab.com/yuccastream/yucca/-/issues/606))
* В форме добавления стрима поле Name не обозначено как обязательное ([#618](https://gitlab.com/yuccastream/yucca/-/issues/618))
* Не паркуются стримы на ARM ([#613](https://gitlab.com/yuccastream/yucca/-/issues/613))
* Не создается превью rtsp стримов на странице создания стрима ([#638](https://gitlab.com/yuccastream/yucca/-/issues/638))
* Некорректно работает нативный пробер на 32 битных системах ([#523](https://gitlab.com/yuccastream/yucca/-/issues/523))
* Некорректное позиционирование бегунка при клике на промежуток недоступности ([#635](https://gitlab.com/yuccastream/yucca/-/issues/635))
* Отключить браузерную автоподстановку в Safari ([#531](https://gitlab.com/yuccastream/yucca/-/issues/531))
* Ошибка при добавлении стрима 500 pq: value too long for type character varying(255) ([#626](https://gitlab.com/yuccastream/yucca/-/issues/626))
* После удаления стрима отображается страница с ошибкой 404 ([#585](https://gitlab.com/yuccastream/yucca/-/issues/585))
* Сохранять в превью картинку только низкого разрешения ([#598](https://gitlab.com/yuccastream/yucca/-/issues/598))

## Что дальше?

Для обновления с 0.3.1 на 0.4.0 ознакомьтесь [с инструкцией по обновлению](/ru/update/0.3.1_to_0.4.0/), инструкции по установке можно найти в разделе [Установка](/ru/install/).

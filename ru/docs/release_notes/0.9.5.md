---
title: Что нового в Yucca 0.9.5
description: Видеонаблюдение для дома и бизнеса
---

# Что нового в Yucca 0.9.5

*26.09.2023*

!!! info "Узнайте больше о функциях Yucca 0.9.0"
	Это технический релиз с исправлениями, читайте обзор новшеств Yucca 0.9.0 [на отдельной странице](/ru/release_notes/0.9.0/).

## Снижение утилизации CPU при большом количестве событий о движении

В тикете 1146 исправлена неработающая дедупликация событий от камер. Это может значительно снижать утилизацию CPU на больших объёмах камер и событий. Ниже пример падения утилизации CPU на ~60ти камерах с включённой детекцией:  

[![issue_1146](/ru/media/v095/issue_1146.png)](/ru/media/v095/issue_1146.png)

## Остальные изменения

### Исправлено

* [server] Высокая утилизация CPU при большом объёме smtp ивентов ([#1146](https://gitlab.com/yuccastream/yucca/-/issues/1146))
* [UI] При переходе на метку по маркеру ивента, бегунок встаёт на начало ближайшую доступность ([#1145](https://gitlab.com/yuccastream/yucca/-/issues/1145))

## Что дальше?

Инструкции по установке можно найти в разделе [Установка](/ru/install/).

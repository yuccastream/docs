---
title: Тестирование производительности на Raspberry Pi 4
description: Видеонаблюдение для дома и бизнеса
---

# Тестирование производительности на Raspberry Pi 4

*29.12.2020.*

[![raspberry-pi-4-label](/ru/media/note/run_on_rpi4/raspberry-pi-4-label.png)](/ru/media/note/run_on_rpi4/raspberry-pi-4-label.png)

Не так давно в Юкка появилась поддержка архитектур ARM и ARM64, и мы сделали тест – сколько камер можно добавить на популярный одноплатный компьютер Raspberry Pi.

## Тестирование

Мы взяли младшую в линейке плату Raspberry Pi 4 с 2 гигабайтами оперативной памяти и плату [Geekworm x832](https://geekworm.com/products/for-raspberry-pi-4-x832-3-5-inch-sata-hdd-storage-expansion-board) для подключения 3,5' HDD. Видео-обзор есть на нашем [Youtube-канале](https://www.youtube.com/watch?v=bF1ogyj0NvU).

Установка на Raspberry Pi и на домашний ПК под управлением Linux не отличаются, нужно только выбрать правильную архитектуру в разделе [документации](/ru/install/Debian_Ubuntu/). Для Raspberry Pi 3 и 4 подойдёт архитектура ARM, а для Raspberry Pi 4 с 8 гигабайтами оперативной памяти выберите ARM64.

После сборки и установки Юкка мы добавили 2 камеры с записью архива на HDD. За сутки получились следующие показатели по утилизации ресурсов:
[![01](/ru/media/note/run_on_rpi4/01.png)](/ru/media/note/run_on_rpi4/01.png)

Показатели жёсткого диска (HDD):
[![02](/ru/media/note/run_on_rpi4/02.png)](/ru/media/note/run_on_rpi4/02.png)

В плане быстродействия работа на Raspberry Pi мало чем отличается от работы на сервере c AMD64 в датацентре:
<!-- [![Видео Работа на Raspberry Pi 4](https://img.youtube.com/vi/d2gPGHzPUFk/0.jpg)](https://www.youtube.com/watch?v=d2gPGHzPUFk) -->
<iframe width="960" height="480" src="https://www.youtube.com/embed/d2gPGHzPUFk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Но кому интересно смотреть на производительность при 2 камерах? Попробуем выжать максимум из платы – добавим ещё 10 потоков.
Общие показатели:
[![03](/ru/media/note/run_on_rpi4/03.png)](/ru/media/note/run_on_rpi4/03.png)

Показатели жёсткого диска (HDD):
[![04](/ru/media/note/run_on_rpi4/04.png)](/ru/media/note/run_on_rpi4/04.png)

Сильно вырос LA, но это нормально так как на каждый поток запускается 2 процесса, много камер - много процессов - много прерываний. Когда LA перевалит за 1 образуется очередь и это повлияет на отзывчивость, но продолжит работать.
Всего я смог добавить 21 камеру:
[![05](/ru/media/note/run_on_rpi4/05.png)](/ru/media/note/run_on_rpi4/05.png)

Архив работает, а вот посмотреть LIVE уже не получилось. Что же на графиках? Там всё плохо – процессор достиг предела – LA больше 5.
Общие показатели:
[![06](/ru/media/note/run_on_rpi4/06.png)](/ru/media/note/run_on_rpi4/06.png)

Показатели жёсткого диска (HDD):
[![07](/ru/media/note/run_on_rpi4/07.png)](/ru/media/note/run_on_rpi4/07.png)

Подведём итоги
Мы не рекомендуем добавлять в Юкка на Raspberry Pi 4 более 10-12 камер, так как это повлияет на работоспособность и отзывчивость ПО, но и такой результат – отличный показатель для одноплатного компьютера за 35$.
Мы уже работаем над усовершенствованной архитектурой и планируем в дальнейшем ещё снизить рабочую нагрузку. Это позволит экономить ресурсы и как следствие добавлять ещё больше камер.

Напоминаем, что существует [платная версия Юкка](https://yucca.app#rate), она может заинтересовать небольших интернет провайдеров, интеграторов видеонаблюдения, средний и крупный бизнес, жилые комплексы, застройщиков и многих других. О дополнительных возможностях и условиях читайте на [сайте](https://yucca.app/). Задать вопрос можно через формы обратной связи, написав на почту [info@yucca.app](mailto:info@yucca.app) или в [Telegram-чат](https://t.me/yuccastream).

---
title: Миграция Sqlite to Postgres
description: Видеонаблюдение для дома и бизнеса
---

# Миграция Sqlite to Postgres

## Обзор

Если вы начали использовать Yucca c Sqlite, а затем ресурсы выросли, вы можете мигрировать данные с базы Sqlite на Postgres. Обратная миграция не предусмотренна.

## Миграция

1. Заранее разверните и подготовьте [postgres](/ru/usage/DB/postgres/).
2. Остановите Yucca сервер:  

    ```sh
    sudo systemctl stop yucca
    ```

3. Запустите команду миграции указав путь к исходной базе и базе назначения:  

    ```sh
    /opt/yucca/yucca admin migrate
    ```

    Список всех параметров можно посмотреть в подсказке:  

    ```sh
    /opt/yucca/yucca admin migrate --help
    Migrate state from SQLite to PostgreSQL

    Usage:
    yucca admin migrate [flags]

    Flags:
        --config string                         Path to configuration file (default "/opt/yucca/yucca.toml")
        --data-dir string                       The data directory used to store state and other persistent data (default "data")
        --database-busy-timeout int             Timeout in seconds for waiting for an SQLite table to become unlocked (default 500)
        --database-ca-cert-path string          The path to the CA certificate to use (default "/etc/ssl/certs")
        --database-cache-mode string            Shared cache setting used for connecting to the database (private, shared) (default "shared")
        --database-client-cert-path string      The path to the client cert
        --database-client-key-path string       The path to the client key
        --database-conn-max-lifetime duration   Sets the maximum amount of time a connection may be reused (default 0s)
        --database-host string                  Database host (not applicable for sqlite3) (default "127.0.0.1:5432")
        --database-max-idle-conn int            The maximum number of connections in the idle connection pool (default 2)
        --database-max-open-conn int            The maximum number of open connections to the database
        --database-name string                  The name of the Yucca database (default "yucca")
        --database-password string              The database user's password (not applicable for sqlite3) (default "postgres")
        --database-path string                  SQLite database location
        --database-ssl-mode string              SSL mode for Postgres (disable, require or verify-full) (default "disable")
        --database-user string                  The database user (not applicable for sqlite3) (default "postgres")
    -h, --help                                  Help for migrate
        --state-dir string                      The directory used to store state and other persistent data (default "<data-dir>/state")
    ```

4. Укажите в [конфигурации](/ru/configuration/#database) новый тип базы данных **postgres** и укажите реквизиты подключения.
5. Запустите Yucca сервер:  

    ```sh
    sudo systemctl start yucca
    ```

6. Убедитесь , что Yucca теперь использует **postgres**, это можно увидеть в разделе конфигурации `/ui/administration/config` или а логе при старте свервера.

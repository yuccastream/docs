---
title: Gateway
description: Видеонаблюдение для дома и бизнеса
---

# Yucca Gateway Настройка

## Запуск Yucca Gateway

Для запуска Yucca Gateway используется тот же бинарный файл Yucca, Enterprise версии, обязательно укажите отдельный каталог в `data-dir`.

!!! note "Инструкции по установке [Yucca](/ru/install/)"

При установке через пакетный менеджер deb/rpm сервис `yucca-gateway` уже установлен в системе, достаточно только запустить

```sh
sudo systemctl status yucca-gateway.service
sudo systemctl enable yucca-gateway.service
sudo systemctl start yucca-gateway.service
sudo systemctl status yucca-gateway.service
```

Если вы устанавливали Yucca иначе, то запустить Gateway можно так:  

```sh
/opt/yucca/yucca gateway --data-dir /opt/yucca/data

```

По умолчанию Yucca Gateway слушает порт `9920`, но вы можете переопределить это поведение. Полный список опций можно посмотреть в подсказке `/opt/yucca/yucca gateway --help`

## Получение токена

Для подключения Yucca Gateway к Yucca Server, ему потребуется токен администратора конечного сервера. Персональный токен можно выпустить в профиле пользователя, в разделе [токенов](/ru/usage/PersonalTokens/).

## Подключение Yucca Gateway к Yucca Server

Для корректной работы, нужно обеспечить сетевую доступность между Yucca Gateway и всеми Yucca Server.  

Предварительно установите `curl`, если его у вас нет.

```sh
sudo apt update && sudo apt install curl -y
```

Все последующие действия можно выполнять c любого хоста, главное чтобы была сетевая доступность.

Пример команды для подключения:

```sh
curl -s -d '{"address": "http://localhost:9910", "token": "p.f22003974f22e41a2c48dc58ac25c24f412bff151"}' http://127.0.0.1:9920/v1/system/gateway/server
```

- `address` - это адрес Yucca Server по которому Yucca Gateway будет к нему обращаться.
- `token` - соответственно токен который мы получили ранее.
- `http://127.0.0.1:9920/v1/system/gateway/server` - адрес Yucca Gateway, на который мы отправляем запрос.

Пример корректного ответа:

```json
{
  "active": true,
  "address": "http://localhost:9910",
  "healthy": true,
  "id": 0,
  "name": "",
  "verified": true
}
```

Таким образом можно добавить несколько узлов Yucca Server.

Всё готово!  
Теперь при авторизации через Yucca Gateway по адресу `http://127.0.0.1:9920`, мы попадём на тот Yucca Server, где существует пользователь с реквизитоми которого мы авторизуемся.

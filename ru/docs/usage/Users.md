---
title: Пользователи
description: Видеонаблюдение для дома и бизнеса
---

# Пользователи

!!! info "💰 Функция доступна по [**подписке**](https://yucca.app/ru/prices) Yucca Plus или Yucca Enterprise"

## Обзор

По умолчанию в Yucca есть только 1 пользователь - администратор. Данная функция позволяет создавать дополнительные учётные записи.

[![users1](/ru/media/features/Users/users1.png)](/ru/media/features/Users/users1.png)

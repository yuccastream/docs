---
title: Туннель
description: Видеонаблюдение для дома и бизнеса
---

# Туннель

!!! info "💰 Функция доступна по [**подписке**](https://yucca.app/ru/prices) Yucca Plus"

## Обзор

Функция позволяет получить доступ к вашей инсталляции в приватной сети без публичного IP-адреса.
Yucca подключается к нашей инфраструктуре и через неё вы получаете доступ к видеорегистратору.

Схема работы выглядит примерно так:

[![schema1](/ru/media/features/Tunnel/schema1.png)](/ru/media/features/Tunnel/schema1.png)

## Пример работы

<video loop autoplay muted>
  <source src="/ru/media/features/Tunnel/tunnel1.mp4" type="video/mp4">
</video>

---
title: Публичные потоки
description: Видеонаблюдение для дома и бизнеса
---

# Публичные потоки

!!! info "💰 Функция доступна по [**подписке**](https://yucca.app/ru/prices) Yucca Plus или Yucca Enterprise"

## Обзор

Позволяет встроить трансляцию с камеры на ваш сайт или сторонний плеер.

<video loop autoplay muted>
  <source src="/ru/media/features/StreamShare/StreamShare1.mp4" type="video/mp4">
</video>

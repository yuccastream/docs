---
title: Брендирование
description: Видеонаблюдение для дома и бизнеса
---

# Брендирование

!!! info "💰 Функция доступна по [**подписке**](https://yucca.app/ru/prices) Yucca Enterprise"

## Обзор

Функция брендирование в Yucca в позволяет заменить логотип, фавикон, заголовок, ссылки на документацию и сайт на свои.  

## Конфигурация

Что бы узнать как настроить брендирование обратитесь к разделу в [конфигурации](/ru/configuration/#branding)

## Примеры брендирования

[![Photoshop1](/ru/media/features/Branding/Photoshop1.png)](/ru/media/features/Branding/Photoshop1.png)
[![Photoshop2](/ru/media/features/Branding/Photoshop2.png)](/ru/media/features/Branding/Photoshop2.png)
[![google1](/ru/media/features/Branding/google1.png)](/ru/media/features/Branding/google1.png)
[![google2](/ru/media/features/Branding/google2.png)](/ru/media/features/Branding/google2.png)
[![hetzner1](/ru/media/features/Branding/hetzner1.png)](/ru/media/features/Branding/hetzner1.png)
[![hetzner2](/ru/media/features/Branding/hetzner2.png)](/ru/media/features/Branding/hetzner2.png)



---
title: Настройка детекции движения на камерах XM
description: Видеонаблюдение для дома и бизнеса
---

# Пример настройки детекции движения по email на камерах XM

!!! info "Камеры этого производителя часто продаются под разными брендами, но их легко узнать по веб интерфейсу, который работает только в Internet Explorer."

Откройте веб интерфейс камеры с правами администратора.

Перейдите в раздел настроек **DeviceCfg (Setting) > System > NetService**

[![xm_01](/ru/media/other/smtp/xm_01.png)](/ru/media/other/smtp/xm_01.png)

---

Дважды кликните на поле **EMAIL**, откроется поле настроек SMTP.

[![xm_02](/ru/media/other/smtp/xm_02.png)](/ru/media/other/smtp/xm_02.png)

---

Укажите реквизиты подключения - *SMTP server*, *port*, *sender* и *receiver*. Последние 2 должны совпадать.
Если в настройках SMTP сервера включена авторизация, укажите *username* и *password*.

Нажмите **Mail Testing**, если всё корректно, вы увидите сообщение об успешно отправленном сообщении `Test message sent success`, в логах yucca сообщение о созданном событии:

```log
Event annotation created, stream_id: 10
```

а в прогресс баре соответствующей камеры появится желтый маркер.

[![xm_03](/ru/media/other/smtp/xm_03.png)](/ru/media/other/smtp/xm_03.png)
[![bar_event](/ru/media/other/smtp/bar_event.png)](/ru/media/other/smtp/bar_event.png)

---

Далее нужно включить непосредственно обнаружение движения.
Перейдите в раздел настроек **DeviceCfg (Setting) > Alarm > Video Motion**

[![xm_04](/ru/media/other/smtp/xm_04.png)](/ru/media/other/smtp/xm_04.png)

---

Включите функцию в опции *enable*, и тип уведомления *Send Email*

[![xm_05](/ru/media/other/smtp/xm_05.png)](/ru/media/other/smtp/xm_05.png)

---

В этой же вкладке нажав на кнопку *Setting*, можно обозначить область видимости в которой нужна детекция движения.

[![xm_06](/ru/media/other/smtp/xm_06.png)](/ru/media/other/smtp/xm_06.png)

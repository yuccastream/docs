---
title: Настройка детекции движения на камерах Dahua
description: Видеонаблюдение для дома и бизнеса
---

# Пример настройки детекции движения по email на камерах Dahua

Откройте веб интерфейс камеры с правами администратора.

Перейдите в раздел настроек **Setting > Network > SMTP (Email)**

[![dahua_01](/ru/media/other/smtp/dahua_01.png)](/ru/media/other/smtp/dahua_01.png)

---

Укажите реквизиты подключения - *SMTP server*, *Port*, *Sender* и *Mail Receiver*. Последние 2 должны совпадать.
Если в настройках SMTP сервера включена авторизация, укажите *username* и *password*.

Нажмите **Test**, если всё корректно, вы увидите сообщение об успешно отправленном сообщении `Succeed`, в логах yucca сообщение о созданном событии:

```log
Event annotation created, stream_id: 10
```

А в прогресс баре соответствующей камеры появится желтый маркер.

[![dahua_02](/ru/media/other/smtp/dahua_02.png)](/ru/media/other/smtp/dahua_02.png)
[![bar_event](/ru/media/other/smtp/bar_event.png)](/ru/media/other/smtp/bar_event.png)

---

Далее нужно включить непосредственно обнаружение движения.
Перейдите в раздел настроек **Setting > Event > Video Detection > Motion Detection**.
Включите функцию в опции *enable*, и тип уведомления *Send Email*.

[![dahua_03](/ru/media/other/smtp/dahua_03.png)](/ru/media/other/smtp/dahua_03.png)

---

В этой же вкладке в поле *Area* нажав на кнопку *Setting*, можно обозначить область видимости в которой нужна детекция движения, а также указать уровень чувствительности.

[![dahua_04](/ru/media/other/smtp/dahua_04.png)](/ru/media/other/smtp/dahua_04.png)

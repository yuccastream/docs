---
title: AuditLogs
description: Видеонаблюдение для дома и бизнеса
---

!!! info "💰 Функция доступна по [**подписке**](https://yucca.app/ru/prices) Yucca Enterprise"

# Журнал аудита

## Обзор

Функция позволяет посматривать действия пользователя и может быть полезна для специалистов информационной безопасности.

[![AuditLogs1](/ru/media/features/AuditLogs/AuditLogs1.png)](/ru/media/features/AuditLogs/AuditLogs1.png)

## Конфигурация

Что бы узнать как настроить журналы аудита обратитесь к разделу в [конфигурации](/ru/configuration/#audit_logs)

---
title: Установка в Docker
description: Видеонаблюдение для дома и бизнеса
---

# Установка в Docker

!!! info "Помощь"
    Если вам нужна помощь специалиста для решения этой или любой другой задачи, изучите наши условия **[технической поддержки](/ru/support/)**.

!!! note "Используйте PostgreSQL, при большим количестве камер"
    При эксплуатации в производственных (production) средах с большим количеством камер (больше 10-ти)
    и включённой детекцией движения мы рекомендуем использовать в качестве базы данных PostgreSQL, как более производительную.
    Как установить и настроить использование, читайте [тут](/ru/usage/DB/postgres/)

??? question "Какую версию установить?"
    Существует 2 редакции Yucca  

    === "Free"
        Полностью бесплатная версия не требует лицензии, и не содержит расширенный функционал.

    === "Plus/Enterprise"
        Содержит расширенный функционал, требует покупки и использования лицензии.  
        Если у вас есть лицензия **Plus** или **Enterprise**, устанавливайте yucca с постфиксом `ent`.

!!! warning "Ошибка 403 Forbidden?"

    Сервис Docker Hub ограничил доступ для пользователей из России, изучите [способы решения проблемы](https://yandex.cloud/ru/docs/troubleshooting/container-registry/known-issues/docker-pull-forbidden-from-russia) от специалистов Яндекса.

    Наиболее простым решением будет добавить префикс в `image`, и скачивать образ через зеркало:

    === "Free"

        ```sh
        mirror.gcr.io/yuccastream/yucca:latest
        ```
    === "Plus/Enterprise"

        ```sh
        mirror.gcr.io/yuccastream/yucca:latest-ent
        ```

## Установка Docker Compose

!!! note "Зависимости"
    Установите [Docker](https://docs.docker.com/engine/install/), быстрая установка в Linux:

    ```shell
    bash <(curl -sSL https://get.docker.com/)
    ```

Скопируйте код в `docker-compose.yaml` файл:

=== "Free"

    ```sh
    ---
    version: "3.8"

    networks:
      yucca_network:

    volumes:
      yucca_data:
      yucca_ffmpeg:

    services:
      yucca:
        image: yuccastream/yucca:latest
        restart: on-failure
        shm_size: "128mb"
        volumes:
          - "yucca_data:/opt/yucca/data"
          - "yucca_ffmpeg:/opt/yucca/ffmpeg"
        networks:
          - yucca_network
        ports:
          - 9910:9910 # Web UI
          - 9912:9912 # Telemetry
          - 1025:1025 # SMTP server
        environment: {}
    ```

=== "Plus/Enterprise"

    ```sh
    ---
    version: "3.8"

    networks:
      yucca_network:

    volumes:
      yucca_data:
      yucca_ffmpeg:

    services:
      yucca:
        image: yuccastream/yucca:latest-ent
        restart: on-failure
        shm_size: "128mb"
        volumes:
          - "yucca_data:/opt/yucca/data"
          - "yucca_ffmpeg:/opt/yucca/ffmpeg"
        networks:
          - yucca_network
        ports:
          - 9910:9910 # Web UI
          - 9912:9912 # Telemetry
          - 1025:1025 # SMTP server
        environment: {}
    ```

Запустите Yucca:

```sh
docker compose up -d
```

После запуска Web-интерфейс будет доступен по адресу <http://ip-вашего-сервера:9910>

## Обновление

Перейдите в каталог с `docker-compose.yml` файлом

Выполните команды:

```bash
docker compose pull
docker compose up -d
```

## Удаление

Перейдите в каталог с `docker-compose.yml` файлом

Выполните команды:

```bash
docker compose down -v
```

будут удалены все файлы, включая архив.

## Дополнительно

* [Системные требования](/ru/usage/SystemRequirements/)
* [Конфигурация](/ru/configuration/)
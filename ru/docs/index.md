---
title: Добро пожаловать
description: Видеонаблюдение для дома и бизнеса
---

# Добро пожаловать

Вы находитесь на сайте с документацией видеорегистратора [Юкка](https://yucca.app/ru) – программного обеспечения для организации видеонаблюдения.

## Быстрый старт

- [Установка](/ru/install/)

## Мобильное приложение

<a href='https://play.google.com/store/apps/details?id=app.yucca.android&utm_source=https%3A%2F%2Fdocs.yucca.app'><img alt='Доступно в Google Play' src='/ru/media/google-play-badge-ru.png' width="300"/></a>
<a href='https://releases.yucca.app/android/latest/app-yucca-release.apk'><img alt='Скачать APK' src='/ru/media/Download_Android_APK_Badge.png' width="300" /></a>

## Улучшить перевод Yucca

Если вы видите опечатку или ошибку в переводе, [предложите её исправление](https://weblate.yucca.app/engage/yucca/).

## Нашли ошибку в тексте?

Если вы увидели ошибку или опечатку, помогите её исправить – напишите нам [на почту](mailto:info@yucca.app), в [Telegram-чат](https://t.me/yuccastream).

#!/bin/bash

set -e
DEBUG=${DEBUG:-false}
if ${DEBUG}; then
    set -x
fi

export CI_REGISTRY_IMAGE=${CI_REGISTRY_IMAGE:-'registry.gitlab.com/yuccastream/docs'}
export CI_COMMIT_SHORT_SHA=${CI_COMMIT_SHORT_SHA:-$(git rev-parse --short HEAD)}
export CI_COMMIT_TAG=${CI_COMMIT_TAG-'latest'}

_help() {
    echo -n -e "Help menu:
build -  Build docker image
push  -  Push docker image
"
}

function build() {
    docker build --force-rm \
        -t ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA} \
        -t ${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG} \
        -f Dockerfile .
}

function push() {
    docker push ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}
    docker push ${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG}
}

case "${1}" in
build) build ;;
push) push ;;
*) _help ;;
esac
